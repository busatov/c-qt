#include "CentralWidget.h"
#include <Qpainter>
#include <QStyleOption>
#include "CustomSwitcher.h"

CentralWidget::CentralWidget(QWidget *parent)
    :QWidget(parent)
{

}

void CentralWidget::paintEvent(QPaintEvent *e)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter1(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter1, this);
}

CentralWidget::~CentralWidget()
{

}

