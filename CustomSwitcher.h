#ifndef CUSTOMSWITCHER_H
#define CUSTOMSWITCHER_H

#include<QPaintEvent>
#include <QWidget>
#include <QGraphicsItem>
#include <QGraphicsView>


class CustomSwitcher : public QGraphicsView
{
    Q_OBJECT

public:
    explicit CustomSwitcher(QWidget *parent);


protected:
    void mousePressEvent(QMouseEvent *event);
    //void mouseMoveEvent(QMouseEvent *event);
    //void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *e);


};

#endif // CUSTOMSWITCHER_H
