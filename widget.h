#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "AnimatedItem.h"
#include "QSlideSwitch.h"
#include "CustomTitleBar.h"
#include <QBoxLayout>
#include <QToolButton>
#include <QLabel>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void maximize();
    void minimize();
    void setStyle();

protected:

private:
    Ui::Widget *ui;
    QSlideSwitch *slider;
    Qt::WindowFlags flags;
    CustomTitleBar *titleBar;
    QVBoxLayout *centralWidgetLayout;
    QHBoxLayout *titleLayout;
    QToolButton *maximize_button;
    QToolButton *minimize_button;
    QToolButton *close_button;
    QToolButton *menu_button;
    QLabel *label;

private:
    void createShadow();
    void constructCentralWidget();
    void createTitleBar();
    void connector();


};

#endif // WIDGET_H
