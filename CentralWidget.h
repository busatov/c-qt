#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

#include<QWidget>
#include <QGraphicsScene>
#include "CustomSwitcher.h"


class CentralWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CentralWidget(QWidget *parent = 0);
    ~CentralWidget();

protected:
    void paintEvent(QPaintEvent *e);
};

#endif // CENTRALWIDGET_H
