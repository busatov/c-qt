#include "AnimatedItem.h"

AnimatedItem::AnimatedItem()
{
    m_alpha=0;
    color.setRgb(0,0,0);
}

void AnimatedItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
           QWidget *)
{
    //QBrush brush(color, Qt::SolidPattern);
    //painter->setBrush(brush);
    painter->drawEllipse(rect());
}

void AnimatedItem::setAlpha(int alpha)
{   m_alpha=alpha;
    color.setRgb(0,0,m_alpha);
}
