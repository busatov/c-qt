#ifndef QSLIDESWITCH_P_H
#define QSLIDESWITCH_P_H

#include "QSlideSwitch.h"
#include <QRect>

class QPainter;
class QPropertyAnimation;

class QSlideSwitchPrivate : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qreal position READ position WRITE setPosition)
    Q_PROPERTY(qreal color READ color WRITE setColor)

public:
    QSlideSwitchPrivate(QSlideSwitch* q);
    ~QSlideSwitchPrivate();
    qreal position() const;
    qreal color() const;
    void setPosition(qreal value);
    void setColor(qreal value);
    void drawSlider(QPainter *painter);
    void updateSliderRect(const QSize& size);

public slots:
    void animate(bool);

public:
    QPropertyAnimation *animation1;
    QPropertyAnimation *animation2;
    bool checked;

private:
    qreal pos;
    qreal col;
    QRectF sliderShape;
    QLinearGradient gradient;
    QSlideSwitch *q_ptr;
    QColor mColor;
};

#endif // QSLIDESWITCH_P_H
