#ifndef ANIMATEDITEM_H
#define ANIMATEDITEM_H

#include <QGraphicsWidget>
#include <QPainter>

class AnimatedItem: public QGraphicsWidget
{

    Q_PROPERTY(int alpha READ alpha WRITE setAlpha)

public:
    AnimatedItem();
    int alpha() const
        {return m_alpha;}
    void setAlpha(int alpha);
private:
    int m_alpha;
    QBrush *brush;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *,
               QWidget *);
    QColor color;

};

#endif // ANIMATEDITEM_H
