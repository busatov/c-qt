#include "widget.h"
#include "ui_widget.h"
#include <QGraphicsDropShadowEffect>
#include <QDesktopWidget>
#include <QSizeGrip>
#include <QMenu>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    flags=0;
    setAttribute(Qt::WA_TranslucentBackground,true);
    createShadow();
    constructCentralWidget();
    createTitleBar();
    connector();
}

void Widget::createShadow()
{
    //create shadow for central widget
    QGraphicsDropShadowEffect *bodyShadow = new QGraphicsDropShadowEffect;
    centralWidgetLayout = new QVBoxLayout(ui->central_widget);
    bodyShadow->setBlurRadius(9.0);
    bodyShadow->setColor(QColor(0, 0, 0, 160));
    bodyShadow->setOffset(0.0);
    ui->central_widget->setGraphicsEffect(bodyShadow);
}

void Widget::constructCentralWidget()
{
    //construct central widget
    QFont font;
    font.setFamily(QStringLiteral("AlternateGotNo3D"));
    font.setPointSize(12);
    QLabel *widgetLabel = new QLabel(ui->central_widget);
    widgetLabel->setStyleSheet("color : rgb(204,204,190); ");
    widgetLabel->setText("USE SLIDER TO SWITCH WINDOW STATES");
    widgetLabel->setFont(font);
    centralWidgetLayout->addWidget(widgetLabel,0,Qt::AlignCenter);
    slider = new QSlideSwitch(this);
    slider->setCheckable(true);
    centralWidgetLayout->addWidget(slider);
    connect(slider,SIGNAL(clicked()),this, SLOT(setStyle()));
    ui->central_widget->setLayout(centralWidgetLayout);
    centralWidgetLayout->addWidget(new QSizeGrip(ui->central_widget), 0, Qt::AlignBottom | Qt::AlignRight);
}

void Widget::createTitleBar()
{
    //create title bar
    titleBar = new CustomTitleBar(this);
    titleBar->setFixedHeight(30);
    titleBar->setStyleSheet("background-color:rgb(248, 248, 242);");
    QGraphicsDropShadowEffect *titleShadow = new QGraphicsDropShadowEffect;  //why shadow is dropping over central widget?
    titleShadow->setBlurRadius(9.0);
    titleShadow->setColor(QColor(0, 0, 0, 160));
    titleShadow->setOffset(0.0);
    titleBar->setGraphicsEffect(titleShadow);

    //create title bar buttons
    menu_button = new QToolButton(titleBar);
    close_button = new QToolButton(titleBar);
    maximize_button = new QToolButton(titleBar);
    minimize_button = new QToolButton(titleBar);
    label = new QLabel(titleBar);

    //add buttons properties
    menu_button->setFixedSize(20,20);
    close_button->setFixedSize(20,20);
    maximize_button->setFixedSize(20,20);
    minimize_button->setFixedSize(20,20);
    menu_button->setIcon(QIcon(":/images/custom_icon.png"));
    close_button->setIcon(QIcon(":/images/close.png"));
    maximize_button->setIcon(QIcon(":/images/maximize.png"));
    minimize_button->setIcon(QIcon(":/images/minimize.png"));
    menu_button->setAutoRaise(true);
    close_button->setAutoRaise(true);
    maximize_button->setAutoRaise(true);
    minimize_button->setAutoRaise(true);
    QFont font;
    font.setFamily(QStringLiteral("AlternateGotNo3D"));
    font.setPointSize(12);
    label->setFont(font);
    label->setText("Custom Window");
    QMenu *menu = new QMenu();
    QAction *testAction = new QAction("exit", this);
    connect(testAction, SIGNAL(triggered()), this, SLOT(close()));
    menu->addAction(testAction);
    menu_button->setMenu(menu);
    menu_button->setPopupMode(QToolButton::InstantPopup);

    //add buttons to the title bar
    titleLayout = new QHBoxLayout(titleBar);
    titleLayout->addWidget(menu_button);
    titleLayout->addWidget(label);
    titleLayout->addWidget(minimize_button);
    titleLayout->addWidget(maximize_button);
    titleLayout->addWidget(close_button);
    titleBar->setLayout(titleLayout);
}

void Widget::connector()
{
    connect(close_button, SIGNAL(clicked()), this, SLOT(close()));
    connect(maximize_button, SIGNAL(clicked()), this, SLOT(maximize()));
    connect(minimize_button, SIGNAL(clicked()), this, SLOT(minimize()));
    ui->verticalLayout->insertWidget(0,titleBar);
    titleBar->setVisible(false);
}

void Widget::setStyle()
{

    if((slider->isChecked()))
    {
        flags |= Qt::FramelessWindowHint;
        setWindowFlags(flags);
        ui->verticalLayout->setMargin(5);
        titleBar->setVisible(true);
        show();

    }

        else
    {
        flags = 0;
        setWindowFlags(flags);
        setAttribute(Qt::WA_TranslucentBackground,false);
        ui->verticalLayout->setMargin(0);
        titleBar->setVisible(false);
        show();
    }
}

void Widget::maximize()
{
    if(this->isMaximized())
    {
        maximize_button->setIcon(QIcon(":/images/maximize.png"));
        this->showNormal();
        ui->verticalLayout->setMargin(9);
    }
    else
    {
        maximize_button->setIcon(QIcon(":/images/restore.png"));
        this->showMaximized();
        QDesktopWidget desktop;
        resize(desktop.availableGeometry().width(), desktop.availableGeometry().height());
        move(0,0);
        ui->verticalLayout->setMargin(0);
    }

}

void Widget::minimize()
{
    this->showMinimized();
}

Widget::~Widget()
{
    delete ui;
}
