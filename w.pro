#-------------------------------------------------
#
# Project created by QtCreator 2014-01-26T15:37:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = w
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    CustomTitleBar.cpp \
    CentralWidget.cpp \
    qslideswitch.cpp

HEADERS  += widget.h \
    CustomTitleBar.h \
    CentralWidget.h \
    QSlideSwitch_p.h \
    QSlideSwitch.h

FORMS    += widget.ui

RESOURCES += \
    images.qrc
