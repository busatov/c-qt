#include "QSlideSwitch_p.h"
#include <QPen>
#include <QBrush>
#include <QPalette>
#include <QPainter>

#include <QPropertyAnimation>

#include <QPaintEvent>
#include <QResizeEvent>

QSlideSwitchPrivate::QSlideSwitchPrivate(QSlideSwitch *q) :
    q_ptr(q), animation1(new QPropertyAnimation(this)), pos(0.0)
    , animation2(new QPropertyAnimation(this)),
    mColor(QColor(204,204,190))
{
    animation1->setTargetObject(this);
    animation1->setPropertyName("position");
    animation1->setStartValue(0.0);
    animation1->setEndValue(1.0);
    animation1->setDuration(300);
    animation1->setEasingCurve(QEasingCurve::InOutExpo);
    animation2->setTargetObject(this);
    animation2->setPropertyName("color");
    animation2->setStartValue(204);
    animation2->setEndValue(255);
    animation2->setDuration(300);
    animation2->setEasingCurve(QEasingCurve::InOutExpo);
}

QSlideSwitchPrivate::~QSlideSwitchPrivate()
{
    delete animation1;
    delete animation2;
}

void QSlideSwitchPrivate::setPosition(qreal value)
{
    pos = value;
    q_ptr->repaint();
}

void QSlideSwitchPrivate::setColor(qreal value)
{
    col=value;
    mColor.setRgb(204,col,190);
    q_ptr->repaint();
}

qreal QSlideSwitchPrivate::position() const
{
    return pos;
}

qreal QSlideSwitchPrivate::color() const
{
    return col;
}

void QSlideSwitchPrivate::updateSliderRect(const QSize& size)
{
    sliderShape.setWidth(size.width() / 2.0);
    sliderShape.setHeight(size.height()-1.0);
}

void QSlideSwitchPrivate::drawSlider(QPainter *painter)
{
    const qreal margin = 3;
    QRectF r = q_ptr->rect().adjusted(0, 0, -1, -1);
    qreal dx = (r.width() - sliderShape.width()) * pos;
    QRectF sliderRect = sliderShape.translated(dx, 0);

    painter->setPen(Qt::NoPen);

    painter->setBrush(mColor);
    painter->drawRoundedRect(r, 70, 70);

    painter->setBrush(QColor(Qt::white));
    painter->drawRoundedRect(sliderRect.adjusted(margin, margin, -margin, -margin), 70, 70);

    if (animation1->state() == QPropertyAnimation::Running)
        return;

    QFont font = q_ptr->font();
    painter->setFont(font);
    painter->setPen(QPen(QBrush(Qt::white), 0));
    if (q_ptr->isChecked())
    {
        painter->drawText(0, 0, r.width()/2, r.height()-1, Qt::AlignCenter, tr("ON"));
    }
    else
    {
        painter->drawText(r.width()/2, 0, r.width()/2, r.height()-1, Qt::AlignCenter, tr("OFF"));
    }
}

void QSlideSwitchPrivate::animate(bool checked)
{
    animation1->setDirection(checked ? QPropertyAnimation::Forward : QPropertyAnimation::Backward);
    animation1->start();
    animation2->setDirection(checked ? QPropertyAnimation::Forward : QPropertyAnimation::Backward);
    animation2->start();
}

QSlideSwitch::QSlideSwitch(QWidget *parent /*= 0*/) :
    QAbstractButton(parent), d_ptr(new QSlideSwitchPrivate(this))
{
    connect(this, SIGNAL(clicked(bool)), d_ptr, SLOT(animate(bool)));
    connect(d_ptr->animation1, SIGNAL(finished()), this, SLOT(update()));
    connect(d_ptr->animation2, SIGNAL(finished()), this, SLOT(update()));
}


QSlideSwitch::~QSlideSwitch()
{
    delete d_ptr;
}

QSize QSlideSwitch::sizeHint() const
{
    return QSize(100,150);
}

bool QSlideSwitch::hitButton(const QPoint& p) const
{
    return rect().contains(p);
}

void QSlideSwitch::paintEvent(QPaintEvent *e)
{
    Q_D(QSlideSwitch);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    d->drawSlider(&painter);
}

void QSlideSwitch::resizeEvent(QResizeEvent *e)
{
    Q_D(QSlideSwitch);
    d->updateSliderRect(e->size());
    repaint();
}
