#include <QStyleOption>
#include <QPainter>
#include "CustomTitleBar.h"

CustomTitleBar::CustomTitleBar(QWidget *parent): QWidget(parent)
{
    mousePressed = false;
}


void CustomTitleBar::mousePressEvent(QMouseEvent *event)
{
    mousePressed = true;
    mousePos = event->globalPos();

    QWidget *parent = parentWidget();

    if(parent)
        wndPos = parent->pos();
}

void CustomTitleBar::mouseMoveEvent(QMouseEvent *event)
{
    QWidget *parent = parentWidget();

    if(parent && mousePressed)
        parent->move(wndPos + (event->globalPos() - mousePos));
}

void CustomTitleBar::mouseReleaseEvent(QMouseEvent *event)
{
    mousePressed = false;
}

void CustomTitleBar::paintEvent(QPaintEvent *event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
}

